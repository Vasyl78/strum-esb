# frozen_string_literal: true

require "sneakers/queue"
require "strum/esb/functions"

module QueuePatch
  def subscribe(worker)
    if @opts[:bindings]
      strum_subscribe(worker)
    elsif @opts[:header_bindings]
      header_subscribe(worker)
    else
      super(worker)
    end
  end

  # bindings: {
  #   actions: "get/account",
  #   events: %w[account/update account/delete],
  #   infos: "account"
  #   pipeline: {
  #     name: "pipeline-name",
  #     actions: "get/account",
  #     events: %w[account/update account/delete],
  #     infos: "account"
  #   }
  # }

  def strum_subscribe(worker)
    @opts[:header_bindings] = []
    actions_subscribes([*(@opts[:bindings][:actions] || @opts[:bindings][:action])])
    events_subscribes([*(@opts[:bindings][:events] || @opts[:bindings][:event])])
    infos_subscribes([*(@opts[:bindings][:infos] || @opts[:bindings][:info])])
    notices_subscribes([*(@opts[:bindings][:notices] || @opts[:bindings][:notice])])
    pipeline_subscribes(@opts[:bindings][:pipeline] || @opts[:bindings][:pipelines])
    header_subscribe(worker)
  end

  def pipeline_subscribes(pipelines)
    return if pipelines.nil?

    actions_subscribes([*(pipelines[:actions] || pipelines[:action])], pipeline: pipelines[:name])
    events_subscribes([*(pipelines[:events] || pipelines[:event])], pipeline: pipelines[:name])
    infos_subscribes([*(pipelines[:infos] || pipelines[:info])], pipeline: pipelines[:name])
    notices_subscribes([*(pipelines[:notices] || pipelines[:notice])], pipeline: pipelines[:name])
  end

  def actions_subscribes(actions, **custom_headers)
    actions.each do |_action|
      exchange, action, resource = Strum::Esb::Functions.action_explain(_action)
      raise StandardError "action binding format must be `exchange:action/resource`" unless resource && action

      @opts[:header_bindings] << {
        exchange: exchange,
        arguments: {
          action: action,
          resource: resource
        }.merge(custom_headers)
      }
    end
  end

  def events_subscribes(events, **custom_headers)
    events.each do |_event|
      exchange, resource, event, state = Strum::Esb::Functions.event_explain(_event)
      raise StandardError "binding format must be `exchange:resource/event{/state}`" unless resource && event

      @opts[:header_bindings] << {
        exchange: exchange,
        arguments: {
          resource: resource,
          event: event,
          state: state
        }.merge(custom_headers)
      }
    end
  end

  def infos_subscribes(infos, **custom_headers)
    infos.each do |_info|
      exchange, resource = Strum::Esb::Functions.info_explain(_info)
      raise StandardError "info binding format must be a `exchange:resource`" unless resource

      @opts[:header_bindings] << {
        exchange: exchange,
        arguments: {
          info: resource
        }.merge(custom_headers)
      }
    end
  end

  def notices_subscribes(notices, **custom_headers)
    notices.each do |_notice|
      exchange, resource, notice = Strum::Esb::Functions.notice_explain(_notice)
      raise StandardError "notice binding format must be a `exchange:resource`" unless resource

      @opts[:header_bindings] << {
        exchange: exchange,
        arguments: {
          notice: notice,
          resource: resource
        }.merge(custom_headers)
      }
    end
  end

  def header_subscribe(worker)
    @bunny = @opts[:connection]
    @bunny ||= create_bunny_connection
    @bunny.start

    @channel = @bunny.create_channel
    @channel.prefetch(@opts[:prefetch])

    handler_klass = worker.opts[:handler] || Sneakers::CONFIG.fetch(:handler)
    # Configure options if needed
    if handler_klass.respond_to?(:configure_queue)
      @opts[:queue_options] = handler_klass.configure_queue(@name, @opts[:queue_options])
    end

    queue = @channel.queue(@name, @opts[:queue_options])

    header_bindings = [*@opts[:header_bindings]]
    header_bindings.each do |header_binding|
      exchange_name = header_binding[:exchange] || @opts[:exchange]
      exchange = @channel.exchange(exchange_name, @opts[:exchange_options].merge(type: :headers))
      queue.bind(exchange,
                 arguments: {}.tap do |args|
                              args["x-match"] = :all
                            end
                   .merge(header_binding[:arguments]))
    end

    handler = handler_klass.new(@channel, queue, worker.opts)

    @consumer = queue.subscribe(block: false, manual_ack: @opts[:ack]) do |delivery_info, metadata, msg|
      worker.do_work(delivery_info, metadata, msg, handler)
    end
    nil
  end
end

module Sneakers
  class Queue
    prepend QueuePatch
  end
end
