# frozen_string_literal: true

module Strum
  module Esb
    # Payload serializer/deserializer
    class Serializer
      CONTENT_TYPES = %w[application/json application/x-protobuf].freeze

      %i[serialize deserialize].each do |base_method|
        define_method base_method do |payload, args: {}|
          content_type = args.fetch(:content_type)
          raise ArgumentError, "invalid content_type" unless valid_content_type?(content_type)

          perform_method_name = content_type_method_name(base_method, content_type)
          payload = send(perform_method_name, payload, args: args)
          [payload, true]
        rescue StandardError => e
          Sneakers.logger.error(e.inspect)
          Sneakers.logger.error "Content type is invalid. Message rejected #{args[:headers]} with payload #{payload}"
          [payload, false]
        end
      end

      private

        def valid_content_type?(content_type)
          CONTENT_TYPES.include?(content_type)
        end

        def content_type_method_name(action, content_type)
          "#{action}_#{data_type(content_type)}"
        end

        def data_type(content_type)
          content_type.gsub("-", "/").split("/").last
        end

        def deserialize_json(payload, args: {}) # rubocop:disable Lint/UnusedMethodArgument
          JSON.parse(payload)
        end

        def deserialize_protobuf(payload, args: {})
          action_name = inflector.camelize(args.fetch(:queue_method_name))
          message_class = args.fetch(:grpc_service).rpc_descs[action_name.to_sym].input

          Strum::Esb.config.serializer_conf[:from_proto].call(message_class, payload)
        end

        def serialize_json(payload, args: {}) # rubocop:disable Lint/UnusedMethodArgument
          payload.to_json
        end

        def serialize_protobuf(payload, args: {})
          return payload unless args.key?(:message_class)

          message_class = args.fetch(:message_class)
          Strum::Esb.config.serializer_conf[:to_proto].call(message_class, payload)
        end

        def inflector
          Dry::Inflector.new
        end
    end
  end
end
