# frozen_string_literal: true

module Strum
  module Esb
    class Functions
      class << self
        def action_explain(source)
          exchange, resource_action = source.include?(":") ? source.split(":") : [Strum::Esb.config.action_exchange, source]
          action, resource = resource_action.split("/")
          [exchange, action, resource]
        end

        def event_explain(source)
          exchange, resource_event = source.include?(":") ? source.split(":") : [Strum::Esb.config.event_exchange, source]
          resource, event, state = resource_event.split("/")
          [exchange, resource, event, state || "success"]
        end

        def notice_explain(source)
          exchange, resource_notice = source.include?(":") ? source.split(":") : [Strum::Esb.config.notice_exchange, source]
          resource, notice = resource_notice.split("/")
          [exchange, resource, notice]
        end

        def info_explain(source)
          source.include?(":") ? source.split(":") : [Strum::Esb.config.info_exchange, source]
        end
      end
    end
  end
end
