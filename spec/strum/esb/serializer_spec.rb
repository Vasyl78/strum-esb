# frozen_string_literal: true

RSpec.describe Strum::Esb::Serializer do
  subject { described_class.new }

  let(:rb_message) { { id: 1, message: "message" } }
  let(:proto_message) { "\b\x01\x12\x04name" }
  let(:message_class) { ProtobufMessage }
  let(:grpc_service_class) { ProtobufService }

  context "#serialize" do
    before do
      @result, @valid_result = subject.serialize(payload, args: args)
    end
    let(:args) { { content_type: content_type } }

    context "when content type is json" do
      let(:content_type) { "application/json" }
      let(:payload) { rb_message }

      it "converts payload to JSON" do
        expect(@result).to eq payload.to_json
        expect(@valid_result).to eq true
      end
    end

    context "when content type is protobuf" do
      let(:content_type) { "application/x-protobuf" }
      let(:payload) { rb_message }
      let(:args) { { content_type: content_type, message_class: message_class } }

      it "converts payload to proto" do
        expect(@result).to eq proto_message
        expect(@valid_result).to eq true
      end
    end

    context "when content type is invalid" do
      let(:content_type) { "application/pdf" }
      let(:payload) { "" }

      it "returns invalid result" do
        expect(@result).to eq payload
        expect(@valid_result).to eq false
      end
    end
  end

  context "#deserialize" do
    before do
      @result, @valid_result = subject.deserialize(payload, args: args)
    end

    let(:args) do
      {
        content_type: content_type,
        queue_method_name: queue_method_name
      }
    end

    let(:queue_method_name) { "" }

    context "when content type is json" do
      let(:content_type) { "application/json" }

      context "when payload is JSON" do
        let(:payload) { { id: 1, message: "message" }.to_json }

        it "converts payload from JSON" do
          expect(@result).to eq JSON.parse(payload)
          expect(@valid_result).to eq true
        end
      end

      context "when payload isn't JSON" do
        let(:payload) { "" }

        it "doesn't convert payload from JSON" do
          expect(@result).to eq payload
          expect(@valid_result).to eq false
        end
      end
    end

    context "when content type is protobuf" do
      let(:content_type) { "application/x-protobuf" }
      let(:payload) { proto_message }
      let(:args) do
        {
          content_type: content_type,
          queue_method_name: queue_method_name,
          grpc_service: grpc_service_class
        }
      end

      context "when protobuf file exists" do
        let(:queue_method_name) { "action_update_name_request" }

        it "converts payload to Hash" do
          expect(@result.to_h).to eq rb_message
          expect(@valid_result).to eq true
        end
      end

      context "when protobuf file doesn't exist" do
        let(:queue_method_name) { "some_random_stuff" }

        it "doesn't convert payload to protobuf" do
          expect(@result).to eq payload
          expect(@valid_result).to eq false
        end
      end
    end

    context "when content type is invalid" do
      let(:content_type) { "application/pdf" }
      let(:payload) { "" }

      it "returns invalid result" do
        expect(@result).to eq payload
        expect(@valid_result).to eq false
      end
    end
  end
end
