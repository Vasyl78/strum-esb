# frozen_string_literal: true

RSpec.describe Strum::Esb::Message do
  subject { described_class.new(**publish_args) }

  let(:publish_args) do
    {
      exchange: exchange,
      headers: headers.dup,
      payload: payload,
      exchange_options: exchange_options,
      **args
    }
  end

  let(:exchange) { "strum.actions" }

  let(:headers) do
    {
      resource: "resource",
      action: "action"
    }
  end

  let(:payload) do
    {
      first_key: "first_value",
      second_key: "second_value",
      thirdt_key: "thirdt_value"
    }
  end

  let(:exchange_options) { {} }
  let(:args) do
    { content_type: content_type }
  end
  let(:content_type) { "application/json" }

  let(:channel) { double }
  let(:xchg) { double }

  before(:each) do
    # ============ reset sneakers config =======================================
    Sneakers.clear!
    Sneakers.configure(
      log: $stdout,
      workers: Strum::Esb.config.sneakers_workers,
      hooks: {
        before_fork: -> { Strum::Esb.config.before_fork_hooks.each(&:call) },
        after_fork: -> { Strum::Esb.config.after_fork_hooks.each(&:call) }
      },
      exchange: Strum::Esb.config.exchange,
      exchange_type: "headers"
    )
    Sneakers.middleware.use(Strum::Esb::ThreadVariablesCleaner, nil)
    Sneakers.logger.level = Logger::INFO
    # ============ reset sneakers config =======================================

    Strum::Esb.config.rabbit_channel_pool = channel

    allow(channel).to receive(:with).and_yield(channel)
    allow(channel).to receive(:headers).and_return(xchg)
    allow(xchg).to receive(:publish)
  end

  after(:each) do
    Strum::Esb.config.rabbit_channel_pool = nil
    Strum::Esb.config.before_publish_hooks = []
  end

  describe ".publish" do
    before do
      allow(described_class)
        .to receive(:new)
        .with(
          exchange: exchange,
          headers: headers,
          payload: payload,
          exchange_options: exchange_options,
          **args
        ).and_return(subject)

      allow(subject).to receive(:publish)
    end

    it "initialize instance" do
      described_class.publish(
        exchange: exchange,
        headers: headers,
        payload: payload,
        exchange_options: exchange_options,
        **args
      )

      expect(described_class)
        .to have_received(:new)
        .with(
          exchange: exchange,
          headers: headers,
          payload: payload,
          exchange_options: exchange_options,
          **args
        )
    end

    it "call #publish" do
      described_class.publish(
        exchange: exchange,
        headers: headers,
        payload: payload,
        exchange_options: exchange_options,
        **args
      )

      expect(subject).to have_received(:publish).once
    end
  end

  describe "#publish" do
    it "should publish a message to an exchange" do
      subject.publish

      expect(xchg)
        .to have_received(:publish)
        .with(payload.to_json, content_type: content_type, headers: headers)
    end

    context "when exchange_options specified in Sneakers::CONFIG" do
      before(:each) do
        Sneakers.configure(exchange_options: { passive: true, no_declare: true })
      end

      it "should initialize exchange with options" do
        subject.publish

        expect(channel)
          .to have_received(:headers)
          .with(
            exchange,
            {
              arguments: {},
              auto_delete: false,
              durable: true,
              no_declare: true,
              passive: true,
              type: "headers"
            }
          )
      end
    end

    context "when exchange_options specified during instance initializing" do
      let(:exchange_options) { { passive: true, no_declare: true } }

      it "should initialize exchange with options" do
        subject.publish

        expect(channel)
          .to have_received(:headers)
          .with(
            exchange,
            {
              arguments: {},
              auto_delete: false,
              durable: true,
              no_declare: true,
              passive: true,
              type: "headers"
            }
          )
      end
    end

    context "before hook was set" do
      before(:each) do
        Strum::Esb.config.before_publish_hooks << lambda do |_body, properties|
          properties[:headers].merge!(traceparent: "00-d9a53161682a29e8ac2b0467c7b7d8b4-5013db8d5f6e56e2-01")
        end
      end

      it "should publish a message to an exchange" do
        subject.publish

        expect(xchg)
          .to have_received(:publish)
          .with(
            payload.to_json,
            content_type: content_type,
            headers: headers.merge(traceparent: "00-d9a53161682a29e8ac2b0467c7b7d8b4-5013db8d5f6e56e2-01")

          )
      end
    end

    context "when pipeline is set to thread" do
      before(:each) do
        Thread.current[:pipeline] = "strum-esb-pipeline"
        Thread.current[:pipeline_id] = "strum-esb-pipeline-id"
      end

      it "should publish a message to an exchange with pipeline data" do
        subject.publish

        expect(xchg)
          .to have_received(:publish)
          .with(
            payload.to_json,
            content_type: content_type,
            headers: headers.merge("pipeline" => "strum-esb-pipeline", "pipeline-id" => "strum-esb-pipeline-id")

          )
      end
    end
  end
end
